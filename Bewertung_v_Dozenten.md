Hallo Herr Schulze-Clewing,

ich gebe allen eine kurze Rückmeldung zum Hauptseminar im Hinblick auf die so langsam nahende Bachelorarbeit.

Mir ist natürlich klar, dass sie sich mit den "Quantencomputern" einen schweren Brocken ausgesucht haben. Das wird natürlich auch berücksichtigt.

Bei der Ausarbeitung ist mir insbesondere aufgefallen, dass die Zitation nicht ganz auf dem notwendigen Level war. Das sollten sie sich vor der Bachelorarbeit auf jeden Fall nochmal anschauen, wie man flächendeckend sauber zitiert.  Ansonsten fand ich Folien und Ausarbeitungs großteils gut gelungen!

Viele Grüße,
Michael Kreutzer

== Rückmeldung:

= Vortrag:

   - Vortragsstil war ok, aber evtl. noch etwas unsicher.
   - Geschwindigkeit war ok
   - inhaltliche Verständlichkeit war teils schwierig

  - super, dass sie ein Hand-Out vorbereitet haben!
  - Folien sind einfach aber gut
  - Zeit hat soweit gepasst

  - die Übergänge zwischen Hauptthemen sollten stärker hervorgehoben werden, z.B. durch Ziwschenfolien
  - Gliederungsansicht rechts finde ich sehr gelungen!

   - Thema hat hohes Niveau und ist nicht leicht zu erklären
   - dennoch hätten sie versuchen können, durch Probevorträge und Einarbeitung der Publikumskritik einen flüssigeren Vortrag hin zu bekommen
   - ein Zusammenfassung und Fazit am Ende ist bei wiss. Vorträgen sehr wichtig und sollte nicht vergessen werden!

   - Fachdiskussion war soweit ok

= Ausarbeitung:
   - Struktur, log, Aufbau sind gut
   - Die Argumentation ist logisch aufgebaut, aber an einigen Stellen könnte die Stringenz und die Begründung für die getroffenen Schlussfolgerungen stärker herausgearbeitet werden:
   - R & G sind gut!
   - Schreibstil: ist gut, aber könnte auch noch etwas klarer sein.
   - versuchen sie Wörter wie "ich" oder "man" bzw. die Ich-Form in solchen Arbeiten zu meiden, weil das als umgangssprachlich wahrgenommen wird.
   - Text-Versändlichkeit könnte teils noch etwas besser sein. Sie gehen stellenweise tief ins Detail wobei der umliegende Text weniger tiefgehend ist. Das könnte den Leser dann stellenweise überfordern. Lösung: Detailgrad im Hauptteil möglichst auf einem Niveua halten.

Formalia, Technik des wissenschaftlichen Arbeitens
   - Abschnitt 1 ist immer Einleitung! Vorwort oder Verzeichnisse könnten sie mit Rümischen Ziffern "I, II, .." kennzeichnen.
   - Zitation:
    - Literatur ist gut gewählt
    - normalerweise müssen alle Abbildungen mit Quellenverweisen versehen werden. Gleiches auch bei Formeln.
    - auch beim Text wird teils nicht klar, woher die Infos stammen. Üblich ist es hier z.B. zumindest Absatzweise die verwendeten Quellen anzugeben
    - Zitation ist teils "löchrig". Beispielsweise hätten die Aussagen unter 5.1 zum echten Zufall eine Quellenangabe zum Beleg gut getan.
    - teils fehlende Quellenangaben bei "Technischen Aussagen". z.B. Bei Beschreibung "Doppelspaltexperiment"
        - bei Verweis auf Bücher sollten Seitenzahlen angegeben werden!
   => sonst alles ok

   - der Inhalt hat ein hohes Niveau
   - Visualisierungen werden zielführend für ein besseres Verständnis eingesetzt

=> insgesamt  eine interessante Ausarbeitung, aber auch ein paar Mängel.